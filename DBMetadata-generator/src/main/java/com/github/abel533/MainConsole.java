package com.github.abel533;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.abel533.database.Dialect;
import com.github.abel533.database.IntrospectedTable;
import com.github.abel533.database.SimpleDataSource;
import com.github.abel533.utils.DBMetadataUtils;
import com.github.abel533.utils.JavaBeansUtil;
import com.github.abel533.utils.PropertyConfig;
import com.github.abel533.utils.VelocityInfoOp;

/**
 * @author Yonggang.Liu
 *
 */
public class MainConsole {

	public static void main(String[] args) throws SQLException {
		PropertyConfig.loadPropertyFile("config.properties");
		String url=PropertyConfig.getProperty("jdbc.url");
		String username=PropertyConfig.getProperty("jdbc.username");
		String password=PropertyConfig.getProperty("jdbc.password");
		
		String author=PropertyConfig.getProperty("author");
		String contact=PropertyConfig.getProperty("contact");
		String packagePath=PropertyConfig.getProperty("packagePath");
		String template=PropertyConfig.getProperty("template");
		
		String path=System.getProperty("user.dir")+"/upingan/";
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		String date = sdf.format(new Date()); 

		
		DBMetadataUtils dbUtils = new DBMetadataUtils(new SimpleDataSource(Dialect.MYSQL, url, username, password));

        List<IntrospectedTable> tables = dbUtils.introspectTables(dbUtils.getDefaultConfig());

		for (IntrospectedTable i : tables) {
			String classLower = JavaBeansUtil.getCamelCaseString(i.getName(),false);
			String classUpper = JavaBeansUtil.getCamelCaseString(i.getName(),true);

			Map<String, Object> map = new HashMap<String, Object>();

			map.put("table", i);
			map.put("classLower", classLower);
			map.put("classUpper", classUpper);
			map.put("author", author);
			map.put("contact", contact);
			map.put("packagePath", packagePath);
			map.put("description", i.getRemarks());
			map.put("date", date);
			map.put("template", template);

			VelocityInfoOp.generatorCode("model.vm", map, path + "dto",
					classUpper + "DTO.java");
//			VelocityInfoOp.generatorCode("xml.vm", map, path + "dao"+ File.separator + "sql",
//					classUpper + ".xml");
//			VelocityInfoOp.generatorCode("dao.vm", map, path + "dao",
//					classUpper+ "Dao.java");
//			VelocityInfoOp.generatorCode("daoImpl.vm", map, path + "dao"+ File.separator + "impl",
//					classUpper + "DaoImpl.java");
//			VelocityInfoOp.generatorCode("service.vm", map, path + "service",
//					classUpper + "Service.java");
//			VelocityInfoOp.generatorCode("serviceImpl.vm", map, path + "service" + File.separator + "impl", 
//					classUpper + "ServiceImpl.java");
//			VelocityInfoOp.generatorCode("controller.vm", map, path, 
//					classUpper + "Controller.java");
			VelocityInfoOp.generatorCode("jsp.vm", map, path, 
					i.getName() + ".jsp");
		}
		System.out.println("生成成功");
        
	}

}
