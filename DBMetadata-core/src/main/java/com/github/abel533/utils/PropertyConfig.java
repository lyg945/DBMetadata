package com.github.abel533.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 读取配置文件
 * @author Yonggang.Liu
 *
 */
public class PropertyConfig {
    private static ConcurrentMap<String, Object> properties =  new ConcurrentHashMap<String, Object>();

    private static PropertyConfig config = new PropertyConfig();

    private PropertyConfig(){}

    public static PropertyConfig me(){
        return config;
    }

    public static void loadPropertyFile(String file){
        Properties property = new Properties();
        if (StringUtils.isEmpty(file))
            throw new IllegalArgumentException("Parameter of file can not be blank");
        if (file.contains(".."))
            throw new IllegalArgumentException("Parameter of file can not contains \"..\"");

        InputStream inputStream = null;
        
        try {
            inputStream = new FileInputStream(new File(file));
            property.load(inputStream);
        } catch (Exception eOne) {
            try {
                ClassLoader loader = Thread.currentThread().getContextClassLoader();
                property.load(loader.getResourceAsStream(file));
            } catch (IOException eTwo) {
                throw new IllegalArgumentException("Properties file loading failed: " + eTwo.getMessage());
            }
        }
        finally {
            try {if (inputStream != null) inputStream.close();} catch (IOException e) {e.printStackTrace();}
        }
        if (property != null){
            for(Map.Entry<Object,Object> entry : property.entrySet()){
                properties.put(entry.getKey().toString(), entry.getValue());
            }
        }
    }

    public static String getProperty(String key) {
        if(properties.containsKey(key)){
            return properties.get(key).toString();
        }
        return null;
    }

    public static String getProperty(String key, String defaultValue) {
        if(properties.containsKey(key)){
            return properties.get(key).toString();
        }
        return defaultValue;
    }

    public static Integer getPropertyToInt(String key) {
        Integer resultInt = null;
        String resultStr = getProperty(key);
        if (resultStr != null)
            resultInt =  Integer.parseInt(resultStr);
        return resultInt;
    }

    public static Integer getPropertyToInt(String key, Integer defaultValue) {
        Integer result = getPropertyToInt(key);
        return result != null ? result : defaultValue;
    }

    public static  Boolean getPropertyToBoolean(String key) {
        String resultStr = getProperty(key);
        Boolean resultBool = null;
        if (resultStr != null) {
            if (resultStr.trim().equalsIgnoreCase("true"))
                resultBool = true;
            else if (resultStr.trim().equalsIgnoreCase("false"))
                resultBool = false;
        }
        return resultBool;
    }

    public static Boolean getPropertyToBoolean(String key, boolean defaultValue) {
        Boolean result = getPropertyToBoolean(key);
        return result != null ? result : defaultValue;
    }
}
